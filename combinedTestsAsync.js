describe("Verify whether WebdriverUniversity Contact Us page opens correctly",function(){

    it("Check whether contact us button opens contact us page",function(done){

    return browser
    .url('http://www.webdriveruniversity.com/')
    .setViewportSize({
        height: 500,
        width: 500
    })    .getTitle().then(function(title) {
        console.log('Title is: ' + title);
    })
    .click('#contact-us')
    .pause(5000)
 

    })

    it("Check whether login button opens Login Portal page",function(done){

    return browser
    .url('http://www.webdriveruniversity.com/') 
    .setViewportSize({
        height: 700,
        width: 1200
    }) 
    .click('#login-portal')
    .getTitle().then(function(title) {
        console.log('Title is: ' + title);
    })
    .pause(5000)
})

})